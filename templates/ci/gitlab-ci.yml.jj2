# Start ignoring YAMLLintBear
include: 'https://gitlab.com/coala/mobans/raw/master/templates/ci/gitlab-ci-base.yml'
# Stop ignoring YAMLLintBear

cache:
  paths:
    - $CI_PROJECT_PATH/.cache/pip

{% block test_jobs %}
{% if language == 'python' %}

.common_deps_script: &common_deps_script |
{% if package_module == 'pyprint' %}
  apk add --no-cache espeak
{% endif %}
{% if language == 'python' %}
{% if test_with_alpine %}
  apk add --no-cache git  # for git+ requirements
{% endif %}
  $PYTHON -m pip install setuptools==21 pip==9
  pip install -r test-requirements.txt{%- if dependencies -%}
      {{ ' ' }}-r requirements.txt{% endif %}

{% endif %}

.common_script:
  before_script:
    - *common_deps_script
  script:
    - $PYTHON -m pytest --cov-fail-under=100
    - $PYTHON -m codecov
  retry: 2

{%- for python_version in python_versions %}
{# Convert floats to string #}
{% set python_version = python_version.__str__() %}
{# Trim 3.4.4 to 3.4 #}
{% set python_version = '.'.join(python_version.split('.')[:2]) %}
{% set python_major = python_version[0] %}
{% set python_image = python_version %}
{% if test_with_alpine %}
{% set python_image = python_image + '-alpine' %}
{% endif %}

tests:{{ python_version }}:
  extends: .common_script
  image: python:{{ python_image }}
{% if python_major != '3' %}
  variables:
    PYTHON: python2
{% endif %}
  coverage: '/Total coverage: \d+\.\d+%/'
{% endfor %}
{% endif %}
{% endblock %}
{% block extra_test_jobs %}
{% endblock %}

lint:
  image: python:3.5
  before_script:
    - $PYTHON -m pip install setuptools==21 pip==9
    - pip install 'git+https://github.com/coala/coala#egg=coala'
    - pip install 'git+https://github.com/coala/coala-bears#egg=coala-bears'
  script:
    - coala --ci

moban:
  image: python:3.5
  before_script:
    - pip install moban
{% if name == 'mobans' %}
  script:
    - moban
    - git diff --exit-code
{% else %}
  script:
    - {{ci_directory}}/check_moban.sh
{% endif %}
